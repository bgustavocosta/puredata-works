const length = 620;
const empty = new Array(620).fill(0)
let values = [];

for (let index = 0; index < length; index++) {
  var entry = []
  for (let indexInner = 0; indexInner < length; indexInner++) {
    if(indexInner === index) {
      entry[indexInner] = 1;
    }
    else {
      entry[indexInner] = 0;
    }
  }
  values.push(entry)
  values.push(empty)
}

console.log(values)

